const express = require('express');
const router = express.Router();
const faker = require('faker');
const chance = new require('chance').Chance();
const jsf=require('json-schema-faker');
jsf.extend('chance',()=>chance);
jsf.extend('faker',()=>faker );


var schema = {
	"type": "array",
	"minItems":1,
	"maxItems":1,
	"items":{
		type:'object',
		properties: {
			city: {
          		type: 'string',
          		faker: 'address.city'
       		},
       		temperature: {
          		type: 'integer',
          		"minimum": -10,
  				"maximum": 35
       		},
       		age: {
          		type: 'integer',
          		chance: 'age'
       		},
       		time: {
          		type: 'integer',
          		chance: 'hour'
       		},
       		time_minutes: {
       			type: 'integer',
       			chance: 'minute'
       		},
       		time_type: {
       			type: 'string',
       			chance: 'ampm'
       		}
		},
		required: ['temperature', 'city', 'time', 'time_type', 'time_minutes']
	}
};


/* GET users listing. */
router.get('/', (req, res) => {
	jsf.resolve(schema).then(sample => {
    res.send(sample);
	});
});

module.exports = router;
